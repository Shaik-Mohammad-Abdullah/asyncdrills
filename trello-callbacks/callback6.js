/* Problem 6: Write thanosCardArray function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for all lists simultaneously */

const { resolve } = require("path/posix");
const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

const problem6 = (thanosBoardId) => {
    setTimeout(() => {
        problem1(thanosBoardId)
            .then((thanosBoardData) => {
                return problem2(thanosBoardData[0]["id"])
            })
            .then((thanosListArray) => {
                const thanosCardArray = thanosListArray.map(async (thanosListObject) => {
                    try {
                        const thanosCardObject = await problem3(thanosListObject["id"])
                        return thanosCardObject
                    } catch (err) {
                        console.error(err);
                    }
                })
                return Promise.all(thanosCardArray)
            })
            .then((resolve) => console.log(resolve))
            .catch((err) => console.error(err));
    }, 2000);
}

module.exports = problem6
