/* Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for the Mind and Space lists simultaneously */

const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

const problem5 = (thanosBoardId, listName1, listName2) => {
    setTimeout(() => {
        problem1(thanosBoardId)
            .then((thanosBoardData) => {
                return problem2(thanosBoardData[0]["id"])
            })
            .then((thanosListArray) => {
                const thanosList = thanosListArray.filter((thanosListObject) => thanosListObject["name"] === listName1 || thanosListObject["name"] === listName2)
                return Promise.all([problem3(thanosList[0]["id"]), problem3(thanosList[1]["id"])])
            })
            .then((thanosCardData) => console.log(thanosCardData))
            .catch((err) => console.error(err));
    }, 2000);
}

module.exports = problem5;
