/* Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function. */

const boardsData = require("./data/trello-callbacks/boards.json")

const problem1 = (searchedId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const data = boardsData.filter((board) => board["id"] === searchedId)
            if (data.length > 0) {
                resolve(data)
            } else {
                reject("Not Found in problem1")
            }
        }, 2000);
    })
}

module.exports = problem1
