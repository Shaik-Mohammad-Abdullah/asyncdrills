/* Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function. */

const listData = require("./data/trello-callbacks/lists.json")
const { resolve } = require("path/posix");

const problem2 = (searchedId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (listData[searchedId]) {
                resolve(listData[searchedId])
            } else {
                reject("Not found in Problem 2")
            }
        }, 2000);
    })
}

module.exports = problem2
