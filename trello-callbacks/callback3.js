/* Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function. */

const { rejects } = require("assert");
const { resolve } = require("path/posix");
const cards = require("./data/trello-callbacks/cards.json")

const problem3 = (listId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (cards[listId]) {
                resolve(cards[listId])
            }
            else {
                reject("Not Found in Problem 3");
            }
        }, 2000);
    })
}

module.exports = problem3
