const { resolve } = require("path/posix");
const problem1 = require("../callback1");

problem1("mcu453ed")
    .then((resolve) => console.log(resolve))
    .catch((reject) => console.error(reject))
