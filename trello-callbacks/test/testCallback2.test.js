const { resolve } = require("path/posix");
const problem2 = require("../callback2");

problem2("mcu453ed")
    .then((resolve) => {
        console.log(resolve)
    })
    .catch((reject) => {
        console.error(`Error is ${reject}`)
    })
