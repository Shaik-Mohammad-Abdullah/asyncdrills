const fs = require('fs')
const path = require('path')

const readData = async (fileName) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, "utf-8", (err, contents) => {
            if (err) {
                reject("Error encountered in readData is ", err)
            }
            else {
                resolve(contents)
            }
        })
    })
}

const writeData = async (fileName, contents) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, JSON.stringify(contents, null, 4), (err) => {
            if (err) {
                reject("Error encountered is ", err)
            }
            else {
                resolve(`\n ${fileName} file has been created \n`)
            }
        })
    })
}

const appendFile = async (fileName) => {
    fs.appendFile('./fileName.txt', fileName + ' ', (err) => {
        if (err) {
            console.error(err)
        }
    })
}

const deleteFile = async (fileName) => {
    return new Promise((resolve, reject) => {
        fs.rm(fileName, (err) => {
            if (err) {
                reject("Error in deleting file is ", err)
            }
            else {
                resolve(`\n ${fileName} file has been deleted \n`)
            }
        })
    })
}

const problem2 = () => {
    readData(path.join(__dirname, "data", "lipsum.txt"))
        .then((contents) => {
            appendFile("upperCase.txt")
            return writeData("upperCase.txt", contents.toUpperCase())
        })
        .then((resolve) => {
            console.log(resolve)
            return readData("upperCase.txt")
        })
        .then((upperCaseData) => {
            console.log(upperCaseData)
            let lowerCase = upperCaseData.toLowerCase().split(" ")
            appendFile("lowerCase.txt")
            return writeData("lowerCase.txt", lowerCase)
        })
        .then((resolve) => {
            console.log(resolve)
            return readData("lowerCase.txt")
        })
        .then((lowerCaseData) => {
            let sortedData = JSON.parse(lowerCaseData).sort()
            console.log(sortedData)
            appendFile("sortedData.txt")
            return writeData("sortedData.txt", sortedData)
        })
        .then((resolve) => {
            console.log(resolve)
            return readData("sortedData.txt")
        })
        .then((sortedData) => {
            console.log(sortedData)
            return readData("fileName.txt")
        })
        .then((list) => {
            console.log(list)
            return Promise.all([deleteFile("upperCase.txt"), deleteFile("lowerCase.txt"), deleteFile("sortedData.txt")])
        })
        .then((content) => {
            console.log(content)
            return deleteFile('fileName.txt')
        })
        .then((resolve) => {
            console.log(resolve)
        })
        .catch((err) => {
            console.error(err)
        })
}

problem2()
