// Importing fs module
const fs = require("fs");
const path = require("path/posix");

// Creating a directory
const makeDirectory = async (address) => {
    return new Promise((resolve, reject) => {
        fs.mkdir(address, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve("jsonFilesDirectory has been created!")
            }
        })
    })
}

// Deleting a directory
const deleteDirectory = async (address) => {
    return new Promise((resolve, reject) => {
        fs.rm(address, { recursive: true }, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve("jsonFilesDirectory has been deleted!")
            }
        })
    })

}

// Problem 1
// Create random JSON files
const creation = async (address) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(address, "jgjgj", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("JSON file has been saved!");
            }
        });
    })
}

// Delete those files simultaneously
const deletion = async (address) => {
    return new Promise((resolve, reject) => {
        const fs = require("fs")
        fs.rm(address, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("JSON file has been deleted!");
            }
        });
    })
}

const problem1 = () => {
    makeDirectory(path.join(__dirname, "jsonFilesDirectory"))
        .then((resolve) => {
            console.log(resolve)
            return Promise.all([creation(path.join(__dirname, "jsonFilesDirectory", "f2.json")), creation(path.join(__dirname, "jsonFilesDirectory", "f1.json"))])
        })
        .then((resolve) => {
            resolve.map((element) => console.log(element))
            return Promise.all([deletion(path.join(__dirname, "jsonFilesDirectory", "f1.json")), deletion(path.join(__dirname, "jsonFilesDirectory", "f2.json"))]);
        })
        .then((resolve) => {
            resolve.map((element) => console.log(element))
            return deleteDirectory(path.join(__dirname, "jsonFilesDirectory"))
        })
        .then((resolve) => {
            console.log(resolve)
        })
        .catch((err) => {
            console.error(err)
        })
}

problem1()
